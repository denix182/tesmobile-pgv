import React, {useState} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Button, Input, Box, HStack, Switch, useColorMode} from 'native-base';
import {AuthContext} from '../context/context';

const LoginScreen = ({route, navigation, navigation: {goBack}}) => {
  const {user} = route.params;
  const {Login} = React.useContext(AuthContext);
  const [phone, setPhone] = useState();
  const [toggle, setToggle] = useState(false);
  const [checked, setChecked] = useState(false);
  const {colorMode, toggleColorMode} = useColorMode();
  const [pass, setPass] = useState('');
  const darkMode = () => {
    setToggle(!toggle);
    toggleColorMode();
  };
  return (
    <Box flex="1" bg={colorMode === 'dark' ? '#333' : '#fff'}>
      <ImageBackground
        source={require('../assets/bg.png')}
        style={{height: 168, width: '100%'}}
        resizeMode="contain"
      />
      <Box mx="5" alignItems="center">
        <Text
          style={{
            color: colorMode === 'dark' ? '#fff' : '#333',
            fontWeight: '600',
            fontSize: 24,
          }}>
          Welcome back!
        </Text>
        <Text
          style={{
            color: colorMode === 'dark' ? '#fff' : '#333',
            marginTop: 10,
          }}>
          Login to continue
        </Text>
        <Input
          mt="8"
          placeholder="Phone number"
          value={phone}
          onChangeText={setPhone}
          size="md"
          keyboardType="number-pad"
          borderRadius={5}
        />
        {phone ? (
          <Input
            mt="4"
            placeholder="Passsword"
            value={pass}
            secureTextEntry
            onChangeText={setPass}
            size="md"
            borderRadius={5}
          />
        ) : null}
        {phone ? (
          <Button
            w="100%"
            size="md"
            colorScheme="yellow"
            mt="4"
            onPress={() => Login(phone, pass)}>
            Continue
          </Button>
        ) : (
          <Button
            w="100%"
            size="md"
            colorScheme="gray"
            mt="4"
            isDisabled
            onPress={() => Login()}>
            Continue
          </Button>
        )}
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={{color: '#287CFA', fontSize: 13, marginTop: 10}}>
            Create a new account
          </Text>
        </TouchableOpacity>
        <HStack alignItems="center" mt="2">
          <Text
            style={{
              marginRight: 10,
              color: colorMode === 'dark' ? '#333' : '#fff',
            }}>
            Dark Mode
          </Text>
          {!toggle ? (
            <Switch onToggle={() => darkMode()} />
          ) : (
            <Switch isChecked onToggle={() => darkMode()} />
          )}
        </HStack>
        <Box bg="amber.100" p="5" mt="20">
          <Text style={{color: '#111', fontWeight: '600', marginBottom: 10}}>
            Credential Login
          </Text>
          <Text style={{color: '#333'}}>
            Phone: {user[0]?.phone.replace(/-/g, '')}
          </Text>
          <Text style={{color: '#333'}}>
            Password: {user[0]?.login.password}
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

export default LoginScreen;
