import React, {useState, useRef, useEffect} from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {
  Button,
  Icon,
  Box,
  HStack,
  Input,
  FormControl,
  Switch,
  useColorMode,
} from 'native-base';
import {AuthContext} from '../context/context';
import {Ionicons} from '@native-base/icons';
import {BarPasswordStrengthDisplay} from 'react-native-password-strength-meter';

const RegisterScreen = ({navigation, route}) => {
  const {Register} = React.useContext(AuthContext);
  const {width, height} = useWindowDimensions();
  const inputField = useRef();
  const [toggle, setToggle] = useState(false);
  const [checked, setChecked] = useState(false);
  const {colorMode, toggleColorMode} = useColorMode();
  const [user, setUser] = useState({
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    bod: '',
  });
  const darkMode = () => {
    setToggle(!toggle);
    toggleColorMode();
  };

  return (
    <Box flex="1" bg={colorMode === 'dark' ? 'coolGray.800' : 'warmGray.50'}>
      <ImageBackground
        source={require('../assets/bg.png')}
        style={{height: 168, width: '100%'}}
        resizeMode="contain"
      />
      <Box mx="5" mt="-10" alignItems="center">
        <Text
          style={{
            color: colorMode === 'dark' ? '#fff' : '#111',
            fontWeight: '600',
            fontSize: 24,
          }}>
          Welcome to UI!
        </Text>
        <Text
          style={{
            color: colorMode === 'dark' ? '#fff' : '#111',
            marginTop: 10,
          }}>
          Rgister to get the benefits
        </Text>
        <FormControl>
          <Input
            mt="8"
            placeholder="Username"
            isRequired
            value={user.username}
            useRef={inputField}
            onChangeText={value => setUser({...user, username: value})}
            size="md"
            borderRadius={5}
          />
          {!user.username && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[0]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="Password"
            isRequired
            value={user.password}
            useRef={inputField}
            onChangeText={value => setUser({...user, password: value})}
            size="md"
            secureTextEntry
            borderRadius={5}
          />
          <BarPasswordStrengthDisplay
            password={user.password}
            width={width - 50}
          />
          {!user.password && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[1]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="First Name"
            isRequired
            value={user.firstName}
            useRef={inputField}
            onChangeText={value => setUser({...user, firstName: value})}
            size="md"
            borderRadius={5}
          />
          {!user.firstName && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[2]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="Last Name"
            isRequired
            value={user.lastName}
            useRef={inputField}
            onChangeText={value => setUser({...user, lastName: value})}
            size="md"
            borderRadius={5}
          />
          {!user.lastName && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[3]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="Phone number"
            value={user.phone}
            useRef={inputField}
            onChangeText={value => setUser({...user, phone: value})}
            size="md"
            keyboardType="number-pad"
            borderRadius={5}
          />
          {!user.phone && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[4]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="Email"
            isRequired
            value={user.email}
            useRef={inputField}
            onChangeText={value => setUser({...user, email: value})}
            size="md"
            borderRadius={5}
          />
          {!user.email && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[5]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
          <Input
            mt="2"
            placeholder="Birthday"
            isRequired
            value={user.bod}
            useRef={inputField}
            onChangeText={value => setUser({...user, bod: value})}
            size="md"
            borderRadius={5}
          />
          {!user.bod && checked ? (
            <FormControl.HelperText>
              <Text
                style={{
                  color: 'red',
                  fontSize: 10,
                  textTransform: 'capitalize',
                }}>
                {Object.keys(user)[6]} is required
              </Text>
            </FormControl.HelperText>
          ) : null}
        </FormControl>
        <Button
          w="100%"
          size="md"
          colorScheme="gray"
          mt="4"
          onPress={() => {
            setChecked(!checked);
            Register(user);
          }}>
          Continue
        </Button>
        <HStack alignItems="center" mt="2">
          <Text style={{marginRight: 10}}>Dark Mode</Text>
          {!toggle ? (
            <Switch onToggle={() => darkMode()} />
          ) : (
            <Switch isChecked onToggle={() => darkMode()} />
          )}
        </HStack>
      </Box>
    </Box>
  );
};

export default RegisterScreen;
