import React, {useEffect, useState} from 'react';
import {Alert, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NativeBaseProvider} from 'native-base';
import LoginApp from './screen/login';
import RegisterApp from './screen/register';
import HomeScreen from './screen/home';
import {AuthContext} from './context/context';

const App = () => {
  const [token, setToken] = useState();
  const [user, setUser] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const authContext = React.useMemo(() => {
    return {
      Login: (phone, pass) => {
        checkLogin(phone, pass);
      },
      Register: user => {
        checkRegister(user);
      },
    };
  });

  useEffect(() => {
    fetch('https://randomuser.me/api/', {
      header: {
        'Content-Type': 'application/json',
      },
    })
      .then(respons => respons.json())
      .then(responsJson => setUser(responsJson.results));
  }, [refresh]);

  const checkLogin = (phone, pass) => {
    if (user[0].login.password == pass && user[0].phone == phone) {
      Alert.alert("You've succesfull Login");
      setToken(valid);
    } else {
      Alert.alert(
        'Password / Username Salah',
        'Password / Phone Number Anda Salah',
      );
      setRefresh(!refresh);
    }
  };

  const checkRegister = user => {
    console.log(user);
  };

  const Authentication = createNativeStackNavigator();
  const AuthStack = () => {
    return (
      <Authentication.Navigator>
        <Authentication.Screen
          name="Login"
          options={{headerShown: false}}
          component={LoginApp}
          initialParams={{user}}
        />
        <Authentication.Screen
          name="Register"
          options={{headerShown: false}}
          component={RegisterApp}
        />
      </Authentication.Navigator>
    );
  };

  const HomeScreen = createNativeStackNavigator();
  const HomeStack = () => {
    return (
      <HomeScreen.Navigator>
        <HomeScreen.Screen name="Home" component={HomeScreen} />
      </HomeScreen.Navigator>
    );
  };
  const RootSScreen = createNativeStackNavigator();
  const RootStack = () => {
    return (
      <RootSScreen.Navigator>
        {!token ? (
          <RootSScreen.Screen
            name="AuthComponent"
            component={AuthStack}
            options={{headerShown: false}}
          />
        ) : (
          <RootSScreen.Screen name="LandingComponent" component={HomeStack} />
        )}
      </RootSScreen.Navigator>
    );
  };
  return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        <NativeBaseProvider>
          <RootStack />
        </NativeBaseProvider>
      </AuthContext.Provider>
    </NavigationContainer>
  );
};

export default App;
